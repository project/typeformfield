<?php

namespace Drupal\typeformfield\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'typeformfield_embed' formatter.
 *
 * @FieldFormatter(
 *   id = "typeformfield_embed",
 *   label = @Translation("Typeform Embed"),
 *   field_types = {
 *     "typeformfield"
 *   }
 * )
 */
class TypeformFieldEmbed extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return parent::defaultSettings() + [
      'hide_header' => FALSE,
      'hide_footer' => FALSE,
      'opacity' => 100,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['hide_header'] = [
      '#title' => $this->t('Hide header'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('hide_header'),
    ];

    $form['hide_footer'] = [
      '#title' => $this->t('Hide footer'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('hide_footer'),
    ];

    $form['opacity'] = [
      '#title' => $this->t('Opactiy'),
      '#type' => 'number',
      '#min' => 0,
      '#max' => 100,
      '#step' => 1,
      '#default_value' => $this->getSetting('opacity') ?? 100,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $settings = $this->getSettings();
    $summary = [];
    $summary[] = $this->t('Opacity: %opacity.', ['%opacity' => $settings['opacity']]);
    $summary[] = ($settings['hide_header']) ? $this->t('Hide header.') : $this->t('Show header.');
    $summary[] = ($settings['hide_footer']) ? $this->t('Hide footer.') : $this->t('Show footer.');
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $id = $item->id;
      $elements[$delta] = [
        '#theme' => 'typeformfield_embed',
        '#id' => $id,
        '#settings' => [
          'hide_header' => $this->getSetting('hide_header'),
          'hide_header' => $this->getSetting('hide_footer'),
          'opacity' => $this->getSetting('opacity'),
        ],
      ];
    }

    return $elements;
  }

}
