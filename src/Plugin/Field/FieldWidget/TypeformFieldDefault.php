<?php

namespace Drupal\typeformfield\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'typeformfield_default' widget.
 *
 * @FieldWidget(
 *   id = "typeformfield_default",
 *   label = @Translation("Typeform Default"),
 *   field_types = {
 *     "typeformfield"
 *   }
 * )
 */
class TypeformFieldDefault extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['id'] = [
      '#type' => 'textfield',
      '#title' => t('ID'),
      '#default_value' => $items[$delta]->id ?? NULL,
      '#maxlength' => 255,
    ];

    return $element;
  }

}
